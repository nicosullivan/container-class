# Container Class
Code and notes for a small workshop on Containers for Paycom's Summer Engagement Program

## Outline

### What Are Containers
- A consistent way to deliver software
- A collection of linux utilities
- Encapsulation
- A way to disconnect the running application from the host


### Why Container
- Consistency
- 

#### Types of Containers
- Docker
- podman
- lxc
- Windows Containers

#### Container Terms
- Images
- Container
- Pods
- Registry
- Repository
- Tags

### How To Containers

#### Create a Container
- Front End
- Multi Stage Builds
#### Build a Container
#### Run a Container

#### Repeat for Backend
- Cover FROM scratch
- Layers

#### Share a Container

### Going further
#### Container Orchestrators
- Kubernetes
- Hashicorp Nomad
- Docker Swarm
- OpenShift

### Some Practical Examples
- GitLab CI/CD https://docs.gitlab.com/ee/ci/
- SteamLinuxRuntime https://gitlab.steamos.cloud/steamrt/steam-runtime-tools/-/tree/main/pressure-vessel

#### Future Topics
### Package Management
### kodecloud