package main

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
)

type Game struct {
	Name      string
	Players   int
	Publisher string
}

var games map[string][]Game = map[string][]Game{
	"rpg": {
		{Name: "Dungeons and Dragons: Fifth Edition", Players: 10, Publisher: "Wizards of the Coast"},
		{Name: "Star Wars RPG", Players: 10, Publisher: "EDGE Studios"},
		{Name: "Forbidden Lands", Players: 10, Publisher: "Free League"},
		{Name: "Stars Without Number", Players: 10, Publisher: "Sine Nomine Publishing"},
	},
	"video": {
		{Name: "Jedi: Survivor", Players: 1, Publisher: "Electronic Arts"},
		{Name: "Monster Hunter Rise", Players: 4, Publisher: "Capcom"},
		{Name: "Cyberpunk 2077", Players: 1, Publisher: "CD Projekt Red"},
	},
	"board": {
		{Name: "Village Pillage", Players: 5, Publisher: "Jellybean Games"},
		{Name: "Dragoon", Players: 4, Publisher: "Lay Waste Games"},
	},
	"wargame": {
		{Name: "Star Wars: Legion", Players: 2, Publisher: "Atomic Mass Games"},
		{Name: "Star Wars: Shatterpoint", Players: 2, Publisher: "Atomic Mass Games"},
		{Name: "Warhammer: 40k", Players: 2, Publisher: "Games Workshop"},
		{Name: "Warhammer: Age of Sigmar", Players: 2, Publisher: "Games Workshop"},
	},
}

func main() {
	app := fiber.New()
	app.Use(logger.New())
	app.Use(cors.New())
	app.Get("/games/:type", func(c *fiber.Ctx) error {
		if games[c.Params("type")] == nil {
			return c.SendStatus(fiber.StatusNotFound)
		}

		return c.JSON(games[c.Params("type")])
	})

	app.Listen(":8080")
}
