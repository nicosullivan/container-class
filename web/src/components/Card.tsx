type CardProps = {
  title: string,
  body: string,
}

const Card = ({title, body}: CardProps): JSX.Element => {
  return (
    <div className="card w-96 bg-base-100 shadow-xl">
      <div className="card-body">
        <h2 className="card-title">{title}</h2>
        <p>{body}</p>
      </div>
    </div>
  )
}

export default Card