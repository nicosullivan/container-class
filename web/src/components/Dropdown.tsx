type DropdownProps = {
  options: string[],
  onClick: (value: string) => void
}

const Dropdown = ({ options, onClick }: DropdownProps): JSX.Element => {
  return (
    <div className="dropdown dropdown-end">
      <label tabIndex={0} className="btn m-1">Games</label>
      <ul tabIndex={0} className="dropdown-content z-[1] menu p-2 shadow bg-base-100 rounded-box w-52">
        {options.map((value: string, index: number): JSX.Element => 
          <li onClick={() => onClick(value)} key={index}>
            <a>{value}</a>
          </li>)}
      </ul>
    </div>
  )
}

export default Dropdown