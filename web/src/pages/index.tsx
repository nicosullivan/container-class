import { Inter } from 'next/font/google'
import { useState, useEffect } from 'react'

import Card from '../components/Card'
import Dropdown from '../components/Dropdown'

const inter = Inter({ subsets: ['latin'] })

type Game = {
  Name: string,
  Players: number,
  Publisher: string,
}

type Games = {
  [key: string]: Game[]
}


export default function Home() {
  const [ games, setGames ] = useState<Games>({})
  const [ selectedGameList, setSelectedGameList ] = useState<string>("rpg")

  useEffect((): void => {
    fetch(`http://localhost:8080/games/${selectedGameList}`)
      .then((response) => response.json())
      .then((json) => setGames((games: Games) => {return {...games, [selectedGameList]: json}}))
  }, [selectedGameList])

  return (
    <main
      className={`flex min-h-screen flex-col items-center p-24 ${inter.className}`}
    >
      <div className="z-10 w-full max-w-5xl items-center justify-between font-mono text-sm lg:flex">
        <p className="fixed left-0 top-0 flex w-full justify-center border-b border-gray-300 bg-gradient-to-b from-zinc-200 pb-6 pt-8 backdrop-blur-2xl dark:border-neutral-800 dark:bg-zinc-800/30 dark:from-inherit lg:static lg:w-auto  lg:rounded-xl lg:border lg:bg-gray-200 lg:p-4 lg:dark:bg-zinc-800/30">
          Select a type of game to see stock!
        </p>
        <div className="fixed bottom-0 left-0 flex h-48 w-full items-end justify-center bg-gradient-to-t from-white via-white dark:from-black dark:via-black lg:static lg:h-auto lg:w-auto lg:bg-none">
          <Dropdown options={["rpg", "wargame", "video", "board"]} onClick={(value: string) => setSelectedGameList(value)}/>
        </div>
      </div>
      
      <div className="grid gap-10 text-left lg:grid-cols-2 object-top content-start items-start m-10">
      {games[selectedGameList]?.map((game: Game, index: number) => <Card title={game.Name} body={game.Publisher} key={index}/>)}
      </div>
    </main>
  )
}
